clear; clf; close all
bag = rosbag('/Users/Rushi/EECE5554/Lab4_fusion/bags/mag_circles.bag');
bag2 = rosbag('/Users/Rushi/EECE5554/Lab4_fusion/bags/gps_imu_data.bag'); %untrimmed bag, not able to zone in on when we started doing the circles
bag_imu = select(bag,'Topic','/imu_topic');
struct = readMessages(bag_imu,"DataFormat","struct");

bag_imu2 = select(bag2,'Topic','/imu_topic'); % loading the untrimmed bag 
bag_gps2 = select(bag2,'Topic','/utm_topic');
struct_gps =readMessages(bag_gps2,"DataFormat","struct");
struct2 = readMessages(bag_imu2,"DataFormat","struct");


saveVarsMat = load('mag_trimmed.mat');   % ignore 
% Mx = saveVarsMat.Mx; % <1207x1 double> too many elements
% My = saveVarsMat.My; % <1207x1 double> too many elements


qx = ((cellfun(@(m) double(m.ImuData.Orientation.X),struct2)));  %quaternion representation
qy = ((cellfun(@(m) double(m.ImuData.Orientation.Y),struct2))); 
qz = ((cellfun(@(m) double(m.ImuData.Orientation.Z),struct2))); 
qw = ((cellfun(@(m) double(m.ImuData.Orientation.W),struct2))); 
yawrate = ((cellfun(@(m) double(m.ImuData.AngularVelocity.Z),struct2))); 

%%%%%%%%%Magnetometer data%%%%%%%%%%%%%%%%
Magx = ((cellfun(@(m) double(m.MagData.MagneticField_.X),struct2))); 

Mx = lowpass(Magx,0.001,0.025)   ;                                            
Magy = ((cellfun(@(m) double(m.MagData.MagneticField_.Y),struct2)));

My = lowpass(Magy,0.001,0.025)   ;  
Magz = ((cellfun(@(m) double(m.MagData.MagneticField_.Z),struct2))); 
Easting = (cellfun(@(m) double(m.UtmEasting),struct_gps));
Northing= ((cellfun(@(m) double(m.UtmNorthing),struct_gps)));
Accx = ((cellfun(@(m) double(m.ImuData.LinearAcceleration.X),struct2)));    %in m/s^2; multiply by *(1000/9.81) to get in mg 
Ax = lowpass(Accx,0.0001,0.025)  ;                                              
Accy = ((cellfun(@(m) double(m.ImuData.LinearAcceleration.Y),struct2)));    
Ay = lowpass(Accy,0.0001,0.025);
Accz = ((cellfun(@(m) double(m.ImuData.LinearAcceleration.Z),struct2)));    
Az = lowpass(Accz,0.0001,0.025);

%% PART A: Calibrating Magnetometer for hard and soft iron effects 
Magxcal = Magx(2000:6000,1); %the magnetic field array during calibration priocess
Magycal = Magy(2000:6000,1); %the magnetic field array during calibration process
%%%%%%%%%%%%%%%%fitting my filtered data to this matlab function I found online
%%%%%%%%%%%%%%%% https://www.mathworks.com/matlabcentral/fileexchange/3215-fit_ellipse
B = fit_ellipse(Magxcal,Magycal); 
% Output:   ellipse_t - structure that defines the best fit to an ellipse
%                       a           - sub axis (radius) of the X axis of the non-tilt ellipse
%                       b           - sub axis (radius) of the Y axis of the non-tilt ellipse
%                       phi         - orientation in radians of the ellipse (tilt)
%                       X0          - center at the X axis of the non-tilt ellipse
%                       Y0          - center at the Y axis of the non-tilt ellipse
%                       X0_in       - center at the X axis of the tilted ellipse
%                       Y0_in       - center at the Y axis of the tilted ellipse
%                       long_axis   - size of the long axis of the ellipse
%                       short_axis  - size of the short axis of the ellipse
%                       status      - status of detection of an ellipse

cos_phi = cos( B.phi);         %defining the rotation matrix, where phi is in radians
sin_phi = sin( B.phi );
R = [ cos_phi sin_phi; -sin_phi cos_phi;];
%%%%%%%%%%%%%%%%%%% from fit_ellipse code
% the axes
%     ver_line        = [ [X0 X0]; Y0+b*[-1 1] ];
%     horz_line       = [ X0+a*[-1 1]; [Y0 Y0] ];
%     new_ver_line    = R*ver_line;
%     new_horz_line   = R*horz_line;
X0_in = B.X0_in; Y0_in = B.Y0_in; X0 = B.X0; Y0 = B.Y0; a=B.a; b = B.b;
%I'm plotting these lines to visualise the center of the ellipse and it's major and minor axes
     ver_line        = [ [X0 X0]; Y0+b*[-1 1] ];
     horz_line       = [ X0+a*[-1 1]; [Y0 Y0] ];
     new_ver_line    = R'*ver_line;
     new_horz_line   = R'*horz_line;
     ver_line_        = [ [0 0]; b*[-1 1] ];
     horz_line_       = [ a*[-1 1]; [0 0] ];
     new_ver_line_    = R'*ver_line_;
     new_horz_line_   = R'*horz_line_;

sigma = B.short_axis/B.long_axis;  %soft iron compensation can be thought of as shrinking the ellipse along its major axis
% we do this by multiplying the x values of the data by sigma = ratio of
% minor axis to major axis which essentially scales down the ellipse into a
% circle 
XY = [Magx Magy];
XYnew = XY*R;
XYcorrected = [(XYnew(:,1)*(sigma))-X0_in (XYnew(:,2)-Y0_in)]*R';
 Magxcorrected = XYcorrected(:,1);
 Magycorrected = XYcorrected(:,2);
% Magxcorrected = (Magx-X0_in)*sigma;
% Magycorrected = (Magy -Y0_in);

clf
t = linspace(0,2*pi);             %plotting ellipse https://www.mathworks.com/matlabcentral/answers/86615-how-to-plot-an-ellipse
%%%%%%%%% equation of an ellipse 
x = B.X0_in + B.a*cos(t)*cos(B.phi) - B.b*sin(t)*sin(B.phi);
y = B.Y0_in + B.a*cos(t)*sin(B.phi) + B.b*sin(t)*cos(B.phi);
%%%%%% without the offset 
x_HI = B.a*cos(t)*cos(B.phi) - B.b*sin(t)*sin(B.phi);
y_HI = B.a*cos(t)*sin(B.phi) + B.b*sin(t)*cos(B.phi);
%%%%%% soft iron corrections
x_HISI = (B.a*cos(t)*cos(B.phi) - B.b*sin(t)*sin(B.phi))*sigma;
y_HISI = B.a*cos(t)*sin(B.phi) + B.b*sin(t)*cos(B.phi);

 figure(1)
 hold on 
 plot(x,y,'r','Linewidth',1)       % ellipse representing raw data
%  plot(x_HI,y_HI,'m','Linewidth',1)     % HI ellipse
%  plot(Magxc,Magyc,'b');            % raw data
   plot(x_HISI,y_HISI,'g');  % HISI ellipse 
 plot( new_ver_line(1,:),new_ver_line(2,:),'r' );
 plot( new_horz_line(1,:),new_horz_line(2,:),'r' );
%  plot( new_ver_line_(1,:),new_ver_line_(2,:),'m' );
%  plot( new_horz_line_(1,:),new_horz_line_(2,:),'m' );
 grid on; axis equal
 xlim([-.2 .2])
 ylim([-.2 .2])
  legend('original ellipse w HISI','HISI effects corrected')
%  brush on 
%  linkdata on
 hold off
%%  PART A: Calculating heading from Mag and Accelerometer
 k = length(Magycorrected);
 heading_mag = [];
 for i= 1:k
     heading_mag(i,1) = atan2(Magxcorrected(i,1),Magycorrected(i,1)); %+360*((Magxcorrected(i,1))<0);
 end

heading_mag = heading_mag -pi/2;

heading_gyro = cumtrapz(0.025,yawrate); 
heading_gyro = wrapToPi(heading_gyro);
heading_gyro = heading_gyro + heading_mag(1);
q =[qw qx qy qz];
[yaw, pitch, roll] = quat2angle(q); 
yaw = -yaw;                                                          %yaw = heading from IMU, -ve sign since the quat2angle program takes the z axis as pointing up 


%% Complementary Filter
tau = 0.001; %%time constant, how fast you want the readings to respond
fs = 40;     %% frequency of sampling in Hz
dt = 1/fs;   %%sampling rate 
alpha = 0.95; %this number ranges between 0-1; alpha =1 indicates that only mag readings are trusted 
alpha1 = tau/(tau+dt);
t = linspace(0,length(heading_gyro)/40,length(heading_gyro));
complementary = alpha*lowpass(heading_mag,.01,40) + (1-alpha)*highpass(heading_gyro,0.015,40);
complementary = wrapToPi(complementary);

figure("Name","Heading from IMU vs Complementary Filter")

plot(t,(yaw)); hold on; plot(t,(complementary)); 
% plot(heading_gyro); plot(heading_mag);
legend('imu','filtered gyro +mag, alpha =0.7')%,'gyro','mag');
xlabel('time (s)')
ylabel('heading (rad)')
hold off 
%% PART B: Comparing velocity from GPS and integrated Accelerations

gps =[Easting Northing] ;                              

dgps = diff(gps);                                            
dist = hypot(dgps(:,1), dgps(:,2));   
dist1 = hypot(Easting,Northing);
vel_gps = dist ./ 1;             % m/s   
tgps = linspace(0,length(vel_gps),length(vel_gps));

accx = medfilt1(Accx,10)-.16;
%% Bias removal

accx(9671:end) = accx(9671:end) - mean(accx(9671:10712));
% making acceleration zero when car was stationary
accx(1:1908) = 0; accx(5849:7686) = 0; accx(9685:10666) = 0; accx(16246:17574) = 0; accx(20717:23808)=0; accx(38805:41338) = 0; accx(53088:end) = 0; 
accx(16198:17557) = accx(16198:17557) - mean(accx(16198:17557));
accx(20717:end) = accx(20717:end) - mean(accx(20717:23825));

accx = filloutliers(accx,'nearest');
accx = lowpass(accx,0.001,40);

vx = cumtrapz(0.025,accx);

vx1= detrend(vx,10);
vx1 = vx1 - vx1(1);


figure(3)
plot(tgps,vel_gps); hold on; plot(t,vx1); 
xlabel('time(s)')
ylabel('velocity(m/s)')
legend('velocity from gps','velocity integrated from accelerometer')
hold off;

%% Part C: Acceleration in y comparison 
for i=1:length(yawrate)
Accy_estimate(i) = yawrate(i)*vx1(i); 
end 
figure("Name","Acceleration component estimate in y direction")
plot(t,Accy_estimate); hold on; plot(t,lowpass(Accy,0.01,40))
grid on
hold off
legend('acceleration estimate from velocity and yaw','Acceleration from imu ')
%% Part C: Attempt at getting trajectory from heading and velocity

for i=1:length(complementary)
vely(i) = sin(complementary(i)-(pi/2))*vx1(i);
velx(i) = -cos(complementary(i)-(pi/2))*vx1(i);
end
dead_northing = cumtrapz(0.025,vely) +Northing(1);
dead_easting = cumtrapz(0.025,velx)+Easting(1);

dead_northing_mod = dead_northing(12287:end)-dead_northing(12287);
dead_easting_mod = dead_easting(12287:end)-dead_easting(12287);

 scale = 1;
plot(dead_easting*scale,dead_northing*scale)
hold on; plot(Easting,Northing); %plot(dead_easting,dead_northing); 
hold off
grid on; legend('dead reackoning','gps')
xlabel('easting(m)')
ylabel('northing(m)')






