#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import serial
from std_msgs.msg import Float64
import utm
import math
import pynmea2
from pack1.msg import gps_msg





if __name__ == '__main__':
    rospy.init_node('gps')
    serial_port = rospy.get_param('~port','/dev/ttyACM0')
    serial_baud = rospy.get_param('~baudrate',57600)
    sampling_rate = rospy.get_param('~sampling_rate',5.0)
    port = serial.Serial(serial_port, serial_baud, timeout=3)
    line = port.readline()
    rospy.loginfo("Data is publishing")
    
    msg = gps_msg()
    
    pub = rospy.Publisher("gpspublisher",gps_msg, queue_size=3)
    
    
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            #rospy.loginfo(line)
            sv = str(line)
            #rospy.loginfo(sv)
            
            if sv.startswith("b'$GNGGA"):
               #rospy.loginfo(sv)
               gps = pynmea2.parse(line.decode("utf-8"))
               rospy.loginfo(gps)
               
               qual = gps.gps_qual
               
               
               lat = gps.latitude
               lon = gps.longitude
               
            
               (n, e, z, zl) = utm.from_latlon(lat,lon)
               msg.latitude =xlatm
               msg.longitude = ylonm
               rospy.loginfo(n)
               msg.northing = n 
               rospy.loginfo(e)
               msg.easting = e 
               rospy.loginfo(z)
               msg.zone = z
               rospy.loginfo(zl)
               msg.zoneletter = zl
               rospy.loginfo(a)
               msg.altitude = a    
               
               rospy.loginfo(qual)
               msg.fix = qual     
               pub.publish(msg)
               
               rospy.loginfo(gps)
               
               
                           
    except rospy.ROSInterruptException:
        port.close()
            

