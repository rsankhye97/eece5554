#!/usr/bin/env python
# -*- coding: utf-8 -*-

from email import header
import rospy
import serial
import numpy as np 
from std_msgs.msg import Float64
import math
from sensor_msgs.msg import Imu, MagneticField
import message_filters


def euler_to_quaternion(yaw, pitch, roll):

    x = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - \
        np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    y = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + \
        np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    z = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - \
        np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    w = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + \
        np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

    return [x, y, z, w]
# code from - https://computergraphics.stackexchange.com/questions/8195/how-to-convert-euler-angles-to-quaternions-and-get-the-same-euler-angles-back-fr


if __name__ == '__main__':
    try:
        while not rospy.is_shutdown():
            rospy.init_node('imu')
            serial_port = rospy.get_param('~port', '/dev/ttyUSB1')
            serial_baud = rospy.get_param('~baudrate', 115200)
            sampling_rate = rospy.get_param('~sampling_rate', 40)
            port = serial.Serial(serial_port, serial_baud, timeout=3)
            line = port.readline()
            rospy.loginfo("Data is publishing")

            msg_Imu = Imu()
            msg_Mag = MagneticField()

            pub_Imu = rospy.Publisher("Imu_data", Imu, queue_size=3)
            pub_Mag = rospy.Publisher("Mag_data",MagneticField,queue_size=3)
            line = port.readline()
            rospy.loginfo(line)
            sv = str(line)
            # rospy.loginfo(sv)
            selecteddata = sv.split(',')
            # rospy.loginfo(selecteddata)
            angular = selecteddata[12].split('*')
            # rospy.loginfo(angular)
            yaw = float(selecteddata[1])
            pitch = float(selecteddata[2])
            roll = float(selecteddata[3])
            quat = euler_to_quaternion(yaw, pitch, roll)      #converts sensor euler angle data and stores it as a quat = [e0 e1 e2 e3]
            
            magx = float(selecteddata[4])
            magy = float(selecteddata[5])
            magz = float(selecteddata[6])
            mag = np.array([magx,magy,magz])
            
            acclx = float(selecteddata[7])
            accly = float(selecteddata[8])
            acclz = float(selecteddata[9])
            acc = np.array([acclx,accly,acclz])
            
            gyrox = float(selecteddata[10])
            gyroy = float(selecteddata[11])
            gyroz = float(angular[0])
            gyro = np.array([gyrox, gyroy, gyroz])
            
            # print(gyroz)
                            
            msg_Imu.orientation.x = quat[0]                        #sensor_msgs/Imu 
            msg_Imu.orientation.y = quat[1]
            msg_Imu.orientation.z = quat[2]
            msg_Imu.orientation.w = quat[3]

            msg_Imu.angular_velocity.x = gyrox
            msg_Imu.angular_velocity.y = gyroy
            msg_Imu.angular_velocity.z = gyroz

            msg_Imu.linear_acceleration.x = acclx 
            msg_Imu.linear_acceleration.y = accly 
            msg_Imu.linear_acceleration.z = acclz 

            msg_Imu.header.frame_id = "Gyro and Accelorometer"
            pub_Imu.publish(msg_Imu)

            msg_Mag.header.frame_id = "Magnetic Field"         #sensor_msgs/MagneticField
            msg_Mag.magnetic_field.x = magx
            msg_Mag.magnetic_field.y = magy
            msg_Mag.magnetic_field.z = magz

            pub_Mag.publish(msg_Mag)

            imu_sub = message_filters.Subscriber('imu', Imu)
            mag_sub = message_filters.Subscriber('mag', MagneticField)

            #ts = message_filters.TimeSynchronizer([imu_sub, mag_sub], 1)
            #ts.registerCallback(msg_Imu,msg_Mag)
            #pub_all = rospy.Subscriber("all",ts)
            
            

    except rospy.ROSInterruptException:
        port.close()
