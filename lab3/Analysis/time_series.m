bag_imu  = rosbag('/Users/Rushi/EECE5554/Lab3_IMU/Bags/3.bag')
struct_imu  = readMessages(bag_imu,'DataFormat','struct')
bag_mag  = rosbag('/Users/Rushi/EECE5554/Lab3_IMU/Bags/4.bag')
struct_mag  = readMessages(bag_mag,'DataFormat',"struct") 

Gyrox = ((cellfun(@(m) double(m.AngularVelocity.X),struct_imu))*(180/pi));    %in degrees 
Gx = lowpass(Gyrox,0.00001,0.025);                                            %passband frequency of 0.0001 for all lowpass
Gyroy = ((cellfun(@(m) double(m.AngularVelocity.Y),struct_imu))*(180/pi));    %in degrees 
Gy = lowpass(Gyroy,0.00001,0.025);
Gyroz = ((cellfun(@(m) double(m.AngularVelocity.Z),struct_imu))*(180/pi));    %in degrees 
Gz = lowpass(Gyroz,0.00001,0.025);

Accx = ((cellfun(@(m) double(m.LinearAcceleration.X),struct_imu))*(1000/9.81));    %in mg 
Ax = lowpass(Accx,0.0001,0.025)  ;                                              
Accy = ((cellfun(@(m) double(m.LinearAcceleration.Y),struct_imu))*(1000/9.81));    %in mg
Ay = lowpass(Accy,0.0001,0.025);
Accz = ((cellfun(@(m) double(m.LinearAcceleration.Z),struct_imu))*(1000/9.81));    %in mg 
Az = lowpass(Accz,0.0001,0.025);

Magx = ((cellfun(@(m) double(m.MagneticField_.X),struct_mag)));   
Mx = lowpass(Magx,0.001,0.025)   ;                                               
Magy = ((cellfun(@(m) double(m.MagneticField_.Y),struct_mag)));    
My = lowpass(Magy,0.001,0.025);
Magz = ((cellfun(@(m) double(m.MagneticField_.Z),struct_mag)));  
Mz = lowpass(Magz,0.001,0.025);

clf
h1 = subplot(3,2,1)
hold on 
plot(timeseries(Gx)); plot(timeseries(Gy)); plot(timeseries(Gz))
title('Unfiltered Gyro Timeseries')
axis square 
ylabel('Degree/s')
xlabel('No of data samples, sampled at 40Hz')
hold off
legend('x','y','z')

h2 = subplot(3,2,2)
hold on 
plot(timeseries(Gyrox)); plot(timeseries(Gyroy)); plot(timeseries(Gyroz))
hold off
title('Filtered Gyro Timeseries at passband frequency 0.0001Hz')
ylabel('Degree/s')
xlabel('No of data samples, sampled at 40Hz')
legend('x','y','z')
axis square

h3 = subplot(3,2,3)
hold on 
plot(timeseries(Ax)); plot(timeseries(Ay)); plot(timeseries(Az))
title('Unfiltered Accel Timeseries')
axis square 
ylabel('milli-gs')
xlabel('No of data samples, sampled at 40Hz')
hold off
legend('x','y','z')

h4 = subplot(3,2,4)
hold on 
plot(timeseries(Accx)); plot(timeseries(Accy)); plot(timeseries(Accz))
hold off
title('Filtered Accel Timeseries at passband frequency 0.0001Hz')
ylabel('milli-gs')
xlabel('No of data samples, sampled at 40Hz')
axis square
legend('x','y','z')

h5 = subplot(3,2,5)
hold on 
plot(timeseries(Mx)); plot(timeseries(My)); plot(timeseries(Mz))
title('Unfiltered Mag Timeseries')
axis square 
ylabel('Gauss')
xlabel('No of data samples, sampled at 40Hz')
hold off
legend('x','y','z')

h6 = subplot(3,2,6)
hold on 
plot(timeseries(Magx)); plot(timeseries(Magy)); plot(timeseries(Magz))
hold off
title('Filtered Mag Timeseries at passband frequency 0.0001Hz')
ylabel('Gauss')
xlabel('No of data samples, sampled at 40Hz')
axis square
legend('x','y','z')




