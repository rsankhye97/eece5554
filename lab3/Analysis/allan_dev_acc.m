bag  = rosbag('/Users/Rushi/EECE5554/Lab3_IMU/Bags/fivehour2.bag')
struct  = readMessages(bag,'DataFormat',"struct")

Fs = 40; %sampling frequency
t0 = 1/Fs; %time per sample 

Accx = ((cellfun(@(m) double(m.Pose.LinearAcceleration.X),struct))*(1000/9.81));    %in mg 
Ax = cumsum(Accx, 1)*0.025;                                                   %https://www.mathworks.com/help/matlab/ref/cumsum.html
Accy = ((cellfun(@(m) double(m.Pose.LinearAcceleration.Y),struct))*(1000/9.81));    %in mg
Ay = cumsum(Accy, 1)*0.025;
Accz = ((cellfun(@(m) double(m.Pose.LinearAcceleration.Z),struct))*(1000/9.81));    %in mg
Az = cumsum(Accz, 1)*0.025;

% Calculate the allan variance 
maxNumM = 100;
L = size(Ax, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

tau = m*t0;

avarx = zeros(numel(m), 1); avary = zeros(numel(m), 1); avarz = zeros(numel(m), 1);
for i = 1:numel(m)
    mi = m(i);
    avarx(i,:) = sum( ...
        (Ax(1+2*mi:L) - 2*Ax(1+mi:L-mi) + Ax(1:L-2*mi)).^2, 1);
    avary(i,:) = sum( ...
        (Ay(1+2*mi:L) - 2*Ay(1+mi:L-mi) + Ay(1:L-2*mi)).^2, 1);
    avarz(i,:) = sum( ...
        (Az(1+2*mi:L) - 2*Az(1+mi:L-mi) + Az(1:L-2*mi)).^2, 1);
end
avarx = avarx ./ (2*tau.^2 .* (L - 2*m));
avary = avary ./ (2*tau.^2 .* (L - 2*m));
avarz = avarz ./ (2*tau.^2 .* (L - 2*m));

% Allan deviation
adevx = sqrt(avarx);
adevy = sqrt(avary);
adevz = sqrt(avarz);


figure(2)
loglog(tau, adevx); hold on; loglog(tau, adevy); loglog(tau, adevz); hold off
title('Allan Deviation for Accelerometer')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on
axis equal
legend('x','y','z')
