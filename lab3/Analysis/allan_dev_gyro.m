
bag  = rosbag('/Users/Rushi/EECE5554/Lab3_IMU/Bags/fivehour2.bag')
struct  = readMessages(bag,'DataFormat',"struct")

Fs = 40; %sampling frequency
t0 = 1/Fs; %time per sample 

Gyrox = ((cellfun(@(m) double(m.Pose.AngularVelocity.X),struct))*(180/pi));    %in degrees 
Gx = cumsum(Gyrox, 1)*0.025;                                                   %https://www.mathworks.com/help/matlab/ref/cumsum.html
Gyroy = ((cellfun(@(m) double(m.Pose.AngularVelocity.Y),struct))*(180/pi));    %in degrees 
Gy = cumsum(Gyroy, 1)*0.025;
Gyroz = ((cellfun(@(m) double(m.Pose.AngularVelocity.Z),struct))*(180/pi));    %in degrees 
Gz = cumsum(Gyroz, 1)*0.025;

% Calculate the allan variance 
maxNumM = 100;
L = size(Gx, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

tau = m*t0;

avarx = zeros(numel(m), 1); avary = zeros(numel(m), 1); avarz = zeros(numel(m), 1);
for i = 1:numel(m)
    mi = m(i);
    avarx(i,:) = sum( ...
        (Gx(1+2*mi:L) - 2*Gx(1+mi:L-mi) + Gx(1:L-2*mi)).^2, 1);
    avary(i,:) = sum( ...
        (Gy(1+2*mi:L) - 2*Gy(1+mi:L-mi) + Gy(1:L-2*mi)).^2, 1);
    avarz(i,:) = sum( ...
        (Gz(1+2*mi:L) - 2*Gz(1+mi:L-mi) + Gz(1:L-2*mi)).^2, 1);
end
avarx = avarx ./ (2*tau.^2 .* (L - 2*m));
avary = avary ./ (2*tau.^2 .* (L - 2*m));
avarz = avarz ./ (2*tau.^2 .* (L - 2*m));

% Allan deviation
adevx = sqrt(avarx);
adevy = sqrt(avary);
adevz = sqrt(avarz);


figure(1)
loglog(tau, adevx); hold on; loglog(tau, adevy); loglog(tau, adevz); hold off
title('Allan Deviation Gyro')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on




index = find(tau == 1.1);   % May be multiple indexes, possibly
yDesiredx = adevx(index)     % angle random walk
yDesiredy = adevy(index)
yDesiredz = adevy(index)




