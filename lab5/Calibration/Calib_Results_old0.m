
% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 777.483748761030597 ; 776.229086016665065 ];

%-- Principal point:
cc = [ 380.968612599653113 ; 499.157459697260435 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.165757931626889 ; -0.504661229191932 ; -0.000275449505823 ; 0.001286027227979 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 0.944240473390701 ; 0.989232038219886 ];

%-- Principal point uncertainty:
cc_error = [ 1.119914525141904 ; 1.228221718722063 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.006604950551596 ; 0.025605352238734 ; 0.000624660694076 ; 0.000600377932609 ; 0.000000000000000 ];

%-- Image size:
nx = 756;
ny = 1008;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.209598e+00 ; -2.085649e+00 ; -2.449769e-01 ];
Tc_1  = [ -2.580982e+02 ; -2.650734e+02 ; 8.350325e+02 ];
omc_error_1 = [ 1.570375e-03 ; 1.665063e-03 ; 3.258718e-03 ];
Tc_error_1  = [ 1.234496e+00 ; 1.351858e+00 ; 1.413262e+00 ];

%-- Image #2:
omc_2 = [ 2.271195e+00 ; 1.864079e+00 ; 6.497288e-01 ];
Tc_2  = [ -1.910498e+02 ; -2.942319e+02 ; 7.108671e+02 ];
omc_error_2 = [ 1.747910e-03 ; 1.126757e-03 ; 2.780538e-03 ];
Tc_error_2  = [ 1.093759e+00 ; 1.145641e+00 ; 1.354855e+00 ];

%-- Image #3:
omc_3 = [ -1.749737e+00 ; -1.909638e+00 ; 3.438811e-02 ];
Tc_3  = [ -2.831573e+02 ; -2.979782e+02 ; 8.552840e+02 ];
omc_error_3 = [ 1.410968e-03 ; 1.348355e-03 ; 2.384448e-03 ];
Tc_error_3  = [ 1.249505e+00 ; 1.371102e+00 ; 1.312071e+00 ];

%-- Image #4:
omc_4 = [ 1.908447e+00 ; 1.683765e+00 ; 1.940288e-01 ];
Tc_4  = [ -2.470222e+02 ; -3.806021e+02 ; 8.877746e+02 ];
omc_error_4 = [ 1.527079e-03 ; 1.208770e-03 ; 2.291808e-03 ];
Tc_error_4  = [ 1.328202e+00 ; 1.417043e+00 ; 1.435478e+00 ];

%-- Image #5:
omc_5 = [ 1.965452e+00 ; 1.988803e+00 ; -5.280757e-01 ];
Tc_5  = [ -2.330530e+02 ; -4.653961e+02 ; 1.174868e+03 ];
omc_error_5 = [ 1.357278e-03 ; 1.684820e-03 ; 2.716932e-03 ];
Tc_error_5  = [ 1.746207e+00 ; 1.879273e+00 ; 1.572114e+00 ];

%-- Image #6:
omc_6 = [ 1.342229e+00 ; 2.267272e+00 ; -7.459275e-01 ];
Tc_6  = [ 1.762896e+01 ; -3.907840e+02 ; 1.415305e+03 ];
omc_error_6 = [ 1.119201e-03 ; 1.785346e-03 ; 2.636043e-03 ];
Tc_error_6  = [ 2.051988e+00 ; 2.244400e+00 ; 1.600275e+00 ];

%-- Image #7:
omc_7 = [ 1.620674e+00 ; 2.558817e+00 ; -1.655419e-01 ];
Tc_7  = [ -5.440865e+01 ; -3.531918e+02 ; 1.176983e+03 ];
omc_error_7 = [ 1.670016e-03 ; 2.240906e-03 ; 4.555343e-03 ];
Tc_error_7  = [ 1.705155e+00 ; 1.863044e+00 ; 1.850967e+00 ];

%-- Image #8:
omc_8 = [ 1.591810e+00 ; 2.275958e+00 ; -1.373137e+00 ];
Tc_8  = [ -3.669979e+01 ; -2.857005e+02 ; 1.379786e+03 ];
omc_error_8 = [ 9.683961e-04 ; 2.058889e-03 ; 2.490773e-03 ];
Tc_error_8  = [ 1.994164e+00 ; 2.198672e+00 ; 1.380424e+00 ];

%-- Image #9:
omc_9 = [ 1.268080e+00 ; 2.247445e+00 ; -7.382437e-01 ];
Tc_9  = [ 2.685967e+01 ; -3.517855e+02 ; 1.409752e+03 ];
omc_error_9 = [ 1.111119e-03 ; 1.748513e-03 ; 2.562973e-03 ];
Tc_error_9  = [ 2.033259e+00 ; 2.235840e+00 ; 1.544794e+00 ];

%-- Image #10:
omc_10 = [ 1.265522e+00 ; 2.434200e+00 ; 4.032015e-01 ];
Tc_10  = [ 1.018840e+02 ; -3.444875e+02 ; 1.061926e+03 ];
omc_error_10 = [ 1.481822e-03 ; 1.655795e-03 ; 2.772272e-03 ];
Tc_error_10  = [ 1.541799e+00 ; 1.692580e+00 ; 1.599395e+00 ];

%-- Image #11:
omc_11 = [ -1.943694e+00 ; -1.815646e+00 ; -7.020474e-01 ];
Tc_11  = [ -2.580318e+02 ; -1.972478e+02 ; 8.001947e+02 ];
omc_error_11 = [ 1.339742e-03 ; 1.568389e-03 ; 2.429301e-03 ];
Tc_error_11  = [ 1.180895e+00 ; 1.319890e+00 ; 1.377768e+00 ];

%-- Image #12:
omc_12 = [ -2.107047e+00 ; -1.827928e+00 ; -9.912453e-01 ];
Tc_12  = [ -1.942147e+02 ; -1.338242e+02 ; 7.074847e+02 ];
omc_error_12 = [ 1.201874e-03 ; 1.722944e-03 ; 2.434534e-03 ];
Tc_error_12  = [ 1.048386e+00 ; 1.157570e+00 ; 1.285045e+00 ];

%-- Image #13:
omc_13 = [ 2.423092e+00 ; 1.279444e+00 ; -2.055889e-04 ];
Tc_13  = [ -4.018713e+02 ; -1.505306e+02 ; 1.119858e+03 ];
omc_error_13 = [ 2.163554e-03 ; 1.196399e-03 ; 3.034799e-03 ];
Tc_error_13  = [ 1.641997e+00 ; 1.799488e+00 ; 1.737390e+00 ];

%-- Image #14:
omc_14 = [ 1.619552e+00 ; 1.962812e+00 ; -4.163558e-01 ];
Tc_14  = [ -1.482387e+02 ; -2.633781e+02 ; 1.399074e+03 ];
omc_error_14 = [ 1.555388e-03 ; 1.617031e-03 ; 2.730067e-03 ];
Tc_error_14  = [ 2.006406e+00 ; 2.211280e+00 ; 1.677174e+00 ];

%-- Image #15:
omc_15 = [ 1.885402e+00 ; 2.427738e+00 ; -2.453031e-01 ];
Tc_15  = [ -1.354772e+02 ; -4.451793e+02 ; 1.067952e+03 ];
omc_error_15 = [ 1.436901e-03 ; 1.929716e-03 ; 3.676606e-03 ];
Tc_error_15  = [ 1.571014e+00 ; 1.689176e+00 ; 1.647572e+00 ];

%-- Image #16:
omc_16 = [ -1.443376e+00 ; -2.186015e+00 ; 4.402785e-01 ];
Tc_16  = [ -1.454081e+02 ; -4.238452e+02 ; 1.158835e+03 ];
omc_error_16 = [ 1.504125e-03 ; 1.547858e-03 ; 2.564271e-03 ];
Tc_error_16  = [ 1.695588e+00 ; 1.833613e+00 ; 1.688613e+00 ];

%-- Image #17:
omc_17 = [ -7.977206e-01 ; -2.494605e+00 ; 1.013754e+00 ];
Tc_17  = [ 1.304298e+02 ; -2.200798e+02 ; 1.409215e+03 ];
omc_error_17 = [ 1.308708e-03 ; 1.881300e-03 ; 2.728623e-03 ];
Tc_error_17  = [ 2.012356e+00 ; 2.241229e+00 ; 1.756778e+00 ];

%-- Image #18:
omc_18 = [ -1.348654e+00 ; -1.991410e+00 ; 6.304508e-01 ];
Tc_18  = [ -1.249409e+02 ; -4.318986e+02 ; 1.090397e+03 ];
omc_error_18 = [ 1.480982e-03 ; 1.351139e-03 ; 2.140157e-03 ];
Tc_error_18  = [ 1.605372e+00 ; 1.723134e+00 ; 1.516028e+00 ];

%-- Image #19:
omc_19 = [ -1.078937e+00 ; -2.750711e+00 ; 8.625277e-01 ];
Tc_19  = [ 2.703159e+01 ; -2.927155e+02 ; 1.402975e+03 ];
omc_error_19 = [ 1.362775e-03 ; 1.972522e-03 ; 3.374125e-03 ];
Tc_error_19  = [ 2.006174e+00 ; 2.231951e+00 ; 1.719524e+00 ];

%-- Image #20:
omc_20 = [ 1.335155e+00 ; 2.428843e+00 ; -1.142221e+00 ];
Tc_20  = [ 7.944114e+00 ; -2.792030e+02 ; 1.467672e+03 ];
omc_error_20 = [ 9.739842e-04 ; 2.075093e-03 ; 2.715554e-03 ];
Tc_error_20  = [ 2.107371e+00 ; 2.340041e+00 ; 1.543739e+00 ];

