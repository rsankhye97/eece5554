% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 776.911338561750767 ; 775.447012930435562 ];

%-- Principal point:
cc = [ 381.402955378569288 ; 498.854612575777480 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.159327075798145 ; -0.478769519690224 ; -0.000226112897574 ; 0.001694690455590 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 0.947604903733515 ; 1.000727931235512 ];

%-- Principal point uncertainty:
cc_error = [ 1.081182021099860 ; 1.178203963480687 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.006315215089503 ; 0.024212996492758 ; 0.000601455513799 ; 0.000577707411532 ; 0.000000000000000 ];

%-- Image size:
nx = 756;
ny = 1008;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.210181e+00 ; -2.086370e+00 ; -2.459338e-01 ];
Tc_1  = [ -2.585537e+02 ; -2.647207e+02 ; 8.337762e+02 ];
omc_error_1 = [ 1.469248e-03 ; 1.567440e-03 ; 3.058090e-03 ];
Tc_error_1  = [ 1.190320e+00 ; 1.296029e+00 ; 1.363914e+00 ];

%-- Image #2:
omc_2 = [ 2.270240e+00 ; 1.863641e+00 ; 6.501266e-01 ];
Tc_2  = [ -1.914359e+02 ; -2.939952e+02 ; 7.098443e+02 ];
omc_error_2 = [ 1.654921e-03 ; 1.051924e-03 ; 2.618206e-03 ];
Tc_error_2  = [ 1.053887e+00 ; 1.098895e+00 ; 1.309281e+00 ];

%-- Image #3:
omc_3 = [ -1.750277e+00 ; -1.910183e+00 ; 3.438673e-02 ];
Tc_3  = [ -2.837049e+02 ; -2.976278e+02 ; 8.542880e+02 ];
omc_error_3 = [ 1.336651e-03 ; 1.283145e-03 ; 2.262633e-03 ];
Tc_error_3  = [ 1.204960e+00 ; 1.314781e+00 ; 1.268166e+00 ];

%-- Image #4:
omc_4 = [ 1.908162e+00 ; 1.683393e+00 ; 1.950266e-01 ];
Tc_4  = [ -2.474779e+02 ; -3.802986e+02 ; 8.865317e+02 ];
omc_error_4 = [ 1.448295e-03 ; 1.151517e-03 ; 2.163431e-03 ];
Tc_error_4  = [ 1.281941e+00 ; 1.358634e+00 ; 1.401262e+00 ];

%-- Image #5:
omc_5 = [ 1.965609e+00 ; 1.988524e+00 ; -5.271490e-01 ];
Tc_5  = [ -2.338007e+02 ; -4.650704e+02 ; 1.173606e+03 ];
omc_error_5 = [ 1.277763e-03 ; 1.609697e-03 ; 2.566885e-03 ];
Tc_error_5  = [ 1.684042e+00 ; 1.802616e+00 ; 1.538296e+00 ];

%-- Image #6:
omc_6 = [ 1.342317e+00 ; 2.266896e+00 ; -7.455306e-01 ];
Tc_6  = [ 1.681125e+01 ; -3.903680e+02 ; 1.414301e+03 ];
omc_error_6 = [ 1.049708e-03 ; 1.707087e-03 ; 2.485399e-03 ];
Tc_error_6  = [ 1.980588e+00 ; 2.153682e+00 ; 1.576234e+00 ];

%-- Image #7:
omc_7 = [ 1.620524e+00 ; 2.558512e+00 ; -1.641903e-01 ];
Tc_7  = [ -5.513625e+01 ; -3.527965e+02 ; 1.175553e+03 ];
omc_error_7 = [ 1.549066e-03 ; 2.087441e-03 ; 4.228913e-03 ];
Tc_error_7  = [ 1.644153e+00 ; 1.786862e+00 ; 1.808743e+00 ];

%-- Image #8:
omc_8 = [ 1.591930e+00 ; 2.275199e+00 ; -1.373282e+00 ];
Tc_8  = [ -3.744846e+01 ; -2.851237e+02 ; 1.378774e+03 ];
omc_error_8 = [ 9.055275e-04 ; 1.966922e-03 ; 2.365918e-03 ];
Tc_error_8  = [ 1.923162e+00 ; 2.109143e+00 ; 1.362042e+00 ];

%-- Image #9:
omc_9 = [ 1.268080e+00 ; 2.247035e+00 ; -7.378367e-01 ];
Tc_9  = [ 2.604237e+01 ; -3.513687e+02 ; 1.408810e+03 ];
omc_error_9 = [ 1.043104e-03 ; 1.672740e-03 ; 2.416586e-03 ];
Tc_error_9  = [ 1.962983e+00 ; 2.145340e+00 ; 1.521956e+00 ];

%-- Image #10:
omc_10 = [ 1.264930e+00 ; 2.433498e+00 ; 4.035483e-01 ];
Tc_10  = [ 1.012758e+02 ; -3.440859e+02 ; 1.060792e+03 ];
omc_error_10 = [ 1.395766e-03 ; 1.557175e-03 ; 2.606919e-03 ];
Tc_error_10  = [ 1.486340e+00 ; 1.623516e+00 ; 1.571068e+00 ];

%-- Image #11:
omc_11 = [ -1.944319e+00 ; -1.816556e+00 ; -7.020278e-01 ];
Tc_11  = [ -2.584999e+02 ; -1.970034e+02 ; 7.992702e+02 ];
omc_error_11 = [ 1.262543e-03 ; 1.491324e-03 ; 2.302001e-03 ];
Tc_error_11  = [ 1.138165e+00 ; 1.264913e+00 ; 1.327074e+00 ];

%-- Image #12:
omc_12 = [ -2.107457e+00 ; -1.828726e+00 ; -9.917538e-01 ];
Tc_12  = [ -1.945863e+02 ; -1.335611e+02 ; 7.064552e+02 ];
omc_error_12 = [ 1.133808e-03 ; 1.641715e-03 ; 2.314136e-03 ];
Tc_error_12  = [ 1.009851e+00 ; 1.108965e+00 ; 1.245255e+00 ];

%-- Image #13:
omc_13 = [ 2.422970e+00 ; 1.279283e+00 ; 1.191645e-03 ];
Tc_13  = [ -4.024877e+02 ; -1.501659e+02 ; 1.118216e+03 ];
omc_error_13 = [ 2.024559e-03 ; 1.124764e-03 ; 2.836515e-03 ];
Tc_error_13  = [ 1.584200e+00 ; 1.725440e+00 ; 1.688873e+00 ];

%-- Image #14:
omc_14 = [ NaN ; NaN ; NaN ];
Tc_14  = [ NaN ; NaN ; NaN ];
omc_error_14 = [ NaN ; NaN ; NaN ];
Tc_error_14  = [ NaN ; NaN ; NaN ];

%-- Image #15:
omc_15 = [ 1.885345e+00 ; 2.427417e+00 ; -2.438510e-01 ];
Tc_15  = [ -1.361930e+02 ; -4.448422e+02 ; 1.066517e+03 ];
omc_error_15 = [ 1.339726e-03 ; 1.811090e-03 ; 3.434700e-03 ];
Tc_error_15  = [ 1.514344e+00 ; 1.619868e+00 ; 1.613327e+00 ];

%-- Image #16:
omc_16 = [ -1.443775e+00 ; -2.186308e+00 ; 4.400633e-01 ];
Tc_16  = [ -1.461505e+02 ; -4.234274e+02 ; 1.157410e+03 ];
omc_error_16 = [ 1.422620e-03 ; 1.463178e-03 ; 2.419706e-03 ];
Tc_error_16  = [ 1.634128e+00 ; 1.758575e+00 ; 1.651131e+00 ];

%-- Image #17:
omc_17 = [ -7.982234e-01 ; -2.494678e+00 ; 1.014247e+00 ];
Tc_17  = [ 1.296034e+02 ; -2.195588e+02 ; 1.408015e+03 ];
omc_error_17 = [ 1.237943e-03 ; 1.773687e-03 ; 2.572001e-03 ];
Tc_error_17  = [ 1.939514e+00 ; 2.149140e+00 ; 1.728460e+00 ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ -1.079502e+00 ; -2.751182e+00 ; 8.626030e-01 ];
Tc_19  = [ 2.616262e+01 ; -2.922366e+02 ; 1.401832e+03 ];
omc_error_19 = [ 1.285938e-03 ; 1.846643e-03 ; 3.169193e-03 ];
Tc_error_19  = [ 1.934223e+00 ; 2.140638e+00 ; 1.697852e+00 ];

%-- Image #20:
omc_20 = [ 1.335266e+00 ; 2.428209e+00 ; -1.142457e+00 ];
Tc_20  = [ 7.124430e+00 ; -2.786337e+02 ; 1.466575e+03 ];
omc_error_20 = [ 9.061095e-04 ; 1.973764e-03 ; 2.562278e-03 ];
Tc_error_20  = [ 2.032683e+00 ; 2.244313e+00 ; 1.525359e+00 ];

