#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import serial
from std_msgs.msg import Float64
import utm
import math
from pack1.msg import gps_msg




if __name__ == '__main__':
    rospy.init_node('gps')
    serial_port = rospy.get_param('~port','/dev/ttyACM0')
    serial_baud = rospy.get_param('~baudrate',4800)
    sampling_rate = rospy.get_param('~sampling_rate',5.0)
    port = serial.Serial(serial_port, serial_baud, timeout=3)
    line = port.readline()
    rospy.loginfo("Data is publishing")
    msg = gps_msg()
    pub = rospy.Publisher("gpspublisher",gps_msg, queue_size=3)
    
    
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            #rospy.loginfo(line)
            sv = str(line)
            #rospy.loginfo(sv)
            
            if sv.startswith("b'$GPGGA"):
               #rospy.loginfo(sv)
               
               selecteddata = sv.split(',')
               #rospy.loginfo(selecteddata)
               xlat = float(selecteddata[2])
               if "W" in selecteddata[5]:
                  ylon = float(selecteddata[4])*-1
               else: 
                  ylon =  float(selecteddata[4])
               
               a = float(selecteddata[9])
               #rospy.loginfo(xlat)
               #rospy.loginfo(ylon)
               #rospy.loginfo(a)
               
               #g1 = int(xlat)
               #g2 = int(ylon)
               
               xlatm = xlat/100
               ylonm = ylon/100
               
               #rospy.loginfo(xlatm)
               #rospy.loginfo(ylonm)
               
               
               
               (n, e, z, zl) = utm.from_latlon(xlatm,ylonm)
               msg.latitude =xlatm
               msg.longitude = ylonm
               rospy.loginfo(n)
               msg.northing = n 
               rospy.loginfo(e)
               msg.easting = e 
               rospy.loginfo(z)
               msg.zone = z
               rospy.loginfo(zl)
               msg.zoneletter = zl
               rospy.loginfo(a)
               msg.altitude = a           
               pub.publish(msg)
               
               
               
                           
    except rospy.ROSInterruptException:
        port.close()
            

