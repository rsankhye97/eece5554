;; Auto-generated. Do not edit!


(when (boundp 'pack1::gps_msg)
  (if (not (find-package "PACK1"))
    (make-package "PACK1"))
  (shadow 'gps_msg (find-package "PACK1")))
(unless (find-package "PACK1::GPS_MSG")
  (make-package "PACK1::GPS_MSG"))

(in-package "ROS")
;;//! \htmlinclude gps_msg.msg.html


(defclass pack1::gps_msg
  :super ros::object
  :slots (_latitude _longitude _altitude _easting _northing _zone _zoneletter ))

(defmethod pack1::gps_msg
  (:init
   (&key
    ((:latitude __latitude) 0.0)
    ((:longitude __longitude) 0.0)
    ((:altitude __altitude) 0.0)
    ((:easting __easting) 0.0)
    ((:northing __northing) 0.0)
    ((:zone __zone) 0.0)
    ((:zoneletter __zoneletter) "")
    )
   (send-super :init)
   (setq _latitude (float __latitude))
   (setq _longitude (float __longitude))
   (setq _altitude (float __altitude))
   (setq _easting (float __easting))
   (setq _northing (float __northing))
   (setq _zone (float __zone))
   (setq _zoneletter (string __zoneletter))
   self)
  (:latitude
   (&optional __latitude)
   (if __latitude (setq _latitude __latitude)) _latitude)
  (:longitude
   (&optional __longitude)
   (if __longitude (setq _longitude __longitude)) _longitude)
  (:altitude
   (&optional __altitude)
   (if __altitude (setq _altitude __altitude)) _altitude)
  (:easting
   (&optional __easting)
   (if __easting (setq _easting __easting)) _easting)
  (:northing
   (&optional __northing)
   (if __northing (setq _northing __northing)) _northing)
  (:zone
   (&optional __zone)
   (if __zone (setq _zone __zone)) _zone)
  (:zoneletter
   (&optional __zoneletter)
   (if __zoneletter (setq _zoneletter __zoneletter)) _zoneletter)
  (:serialization-length
   ()
   (+
    ;; float64 _latitude
    8
    ;; float64 _longitude
    8
    ;; float64 _altitude
    8
    ;; float64 _easting
    8
    ;; float64 _northing
    8
    ;; float64 _zone
    8
    ;; string _zoneletter
    4 (length _zoneletter)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _latitude
       (sys::poke _latitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _longitude
       (sys::poke _longitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _altitude
       (sys::poke _altitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _easting
       (sys::poke _easting (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _northing
       (sys::poke _northing (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _zone
       (sys::poke _zone (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; string _zoneletter
       (write-long (length _zoneletter) s) (princ _zoneletter s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _latitude
     (setq _latitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _longitude
     (setq _longitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _altitude
     (setq _altitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _easting
     (setq _easting (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _northing
     (setq _northing (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _zone
     (setq _zone (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; string _zoneletter
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _zoneletter (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get pack1::gps_msg :md5sum-) "832217a2b77a5b5f552e97acefdbd874")
(setf (get pack1::gps_msg :datatype-) "pack1/gps_msg")
(setf (get pack1::gps_msg :definition-)
      "float64 latitude
float64 longitude
float64 altitude
float64 easting
float64 northing 
float64 zone
string zoneletter


")



(provide :pack1/gps_msg "832217a2b77a5b5f552e97acefdbd874")


