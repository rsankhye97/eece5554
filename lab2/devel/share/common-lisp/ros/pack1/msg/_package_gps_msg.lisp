(cl:in-package pack1-msg)
(cl:export '(LATITUDE-VAL
          LATITUDE
          LONGITUDE-VAL
          LONGITUDE
          ALTITUDE-VAL
          ALTITUDE
          EASTING-VAL
          EASTING
          NORTHING-VAL
          NORTHING
          ZONE-VAL
          ZONE
          ZONELETTER-VAL
          ZONELETTER
          FIX-VAL
          FIX
))