// Auto-generated. Do not edit!

// (in-package pack1.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class gps_msg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.latitude = null;
      this.longitude = null;
      this.altitude = null;
      this.easting = null;
      this.northing = null;
      this.zone = null;
      this.zoneletter = null;
      this.fix = null;
    }
    else {
      if (initObj.hasOwnProperty('latitude')) {
        this.latitude = initObj.latitude
      }
      else {
        this.latitude = 0.0;
      }
      if (initObj.hasOwnProperty('longitude')) {
        this.longitude = initObj.longitude
      }
      else {
        this.longitude = 0.0;
      }
      if (initObj.hasOwnProperty('altitude')) {
        this.altitude = initObj.altitude
      }
      else {
        this.altitude = 0.0;
      }
      if (initObj.hasOwnProperty('easting')) {
        this.easting = initObj.easting
      }
      else {
        this.easting = 0.0;
      }
      if (initObj.hasOwnProperty('northing')) {
        this.northing = initObj.northing
      }
      else {
        this.northing = 0.0;
      }
      if (initObj.hasOwnProperty('zone')) {
        this.zone = initObj.zone
      }
      else {
        this.zone = 0.0;
      }
      if (initObj.hasOwnProperty('zoneletter')) {
        this.zoneletter = initObj.zoneletter
      }
      else {
        this.zoneletter = '';
      }
      if (initObj.hasOwnProperty('fix')) {
        this.fix = initObj.fix
      }
      else {
        this.fix = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type gps_msg
    // Serialize message field [latitude]
    bufferOffset = _serializer.float64(obj.latitude, buffer, bufferOffset);
    // Serialize message field [longitude]
    bufferOffset = _serializer.float64(obj.longitude, buffer, bufferOffset);
    // Serialize message field [altitude]
    bufferOffset = _serializer.float64(obj.altitude, buffer, bufferOffset);
    // Serialize message field [easting]
    bufferOffset = _serializer.float64(obj.easting, buffer, bufferOffset);
    // Serialize message field [northing]
    bufferOffset = _serializer.float64(obj.northing, buffer, bufferOffset);
    // Serialize message field [zone]
    bufferOffset = _serializer.float64(obj.zone, buffer, bufferOffset);
    // Serialize message field [zoneletter]
    bufferOffset = _serializer.string(obj.zoneletter, buffer, bufferOffset);
    // Serialize message field [fix]
    bufferOffset = _serializer.int64(obj.fix, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type gps_msg
    let len;
    let data = new gps_msg(null);
    // Deserialize message field [latitude]
    data.latitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [longitude]
    data.longitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [altitude]
    data.altitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [easting]
    data.easting = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [northing]
    data.northing = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [zone]
    data.zone = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [zoneletter]
    data.zoneletter = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [fix]
    data.fix = _deserializer.int64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += _getByteLength(object.zoneletter);
    return length + 60;
  }

  static datatype() {
    // Returns string type for a message object
    return 'pack1/gps_msg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5422ddcbee03e76fb23d5edab5270776';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 latitude
    float64 longitude
    float64 altitude
    float64 easting
    float64 northing 
    float64 zone
    string zoneletter
    int64 fix 
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new gps_msg(null);
    if (msg.latitude !== undefined) {
      resolved.latitude = msg.latitude;
    }
    else {
      resolved.latitude = 0.0
    }

    if (msg.longitude !== undefined) {
      resolved.longitude = msg.longitude;
    }
    else {
      resolved.longitude = 0.0
    }

    if (msg.altitude !== undefined) {
      resolved.altitude = msg.altitude;
    }
    else {
      resolved.altitude = 0.0
    }

    if (msg.easting !== undefined) {
      resolved.easting = msg.easting;
    }
    else {
      resolved.easting = 0.0
    }

    if (msg.northing !== undefined) {
      resolved.northing = msg.northing;
    }
    else {
      resolved.northing = 0.0
    }

    if (msg.zone !== undefined) {
      resolved.zone = msg.zone;
    }
    else {
      resolved.zone = 0.0
    }

    if (msg.zoneletter !== undefined) {
      resolved.zoneletter = msg.zoneletter;
    }
    else {
      resolved.zoneletter = ''
    }

    if (msg.fix !== undefined) {
      resolved.fix = msg.fix;
    }
    else {
      resolved.fix = 0
    }

    return resolved;
    }
};

module.exports = gps_msg;
