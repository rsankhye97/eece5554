

bagmov1 = rosbag('/Users/Rushi/Dropbox/Mac/Downloads/obstructedstationary2.bag');
msgStructs1 = readMessages(bagmov1,'DataFormat','struct');
Easting1 = (cellfun(@(m) double(m.UtmEasting),msgStructs1));
Northing1= ((cellfun(@(m) double(m.UtmNorthing),msgStructs1)));
Altitude1 = cellfun(@(m) double(m.Altitude),msgStructs1);
Fix1 = cellfun(@(m) double(m.Fix),msgStructs1);

bagmov2 = rosbag('/Users/Rushi/Dropbox/Mac/Downloads/clearstationary.bag');
msgStructs2 = readMessages(bagmov2,'DataFormat','struct');
Easting2 = (cellfun(@(m) double(m.UtmEasting),msgStructs2));
Northing2= ((cellfun(@(m) double(m.UtmNorthing),msgStructs2)));
Altitude2 = cellfun(@(m) double(m.Altitude),msgStructs2);


% figure('Name','3d map of Obstructed')
% % b1 = Easting1\Northing1
% % yCalc1 = b1*Easting1;
% scatter3(Easting1,Northing1, Altitude1,'ko')
% 
% SEM = (std(Easting1)*1000)/sqrt(length(Easting1))
% std(Northing1)*1000
% std(Altitude1)*1000
% 
% 
% 
% hold off
% figure('Name','3d map of Clear')
% scatter3(Easting2,Northing2,Altitude2)

figure('Name','Altitude Variation of Obstructed  ')
plot(Altitude1,"ko")
figure('Name','Easting v Northing of Obstructed  ')
scatter(Easting1,Northing1,"ko")
% p = polyfit(Easting1, Northing1, 3)
% v = polyval(p, Easting1)
% hold on
% plot(Easting1,v)
% hold off


figure('Name','Altitude Variation of Obstructed  ')
plot(Altitude2,"ko")