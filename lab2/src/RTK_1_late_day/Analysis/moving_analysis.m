clear; clc; clf; close all

bagmov1 = rosbag('/Users/Rushi/EECE5554/Lab2/obstructedmoving2.bag');
msgStructs1 = readMessages(bagmov1,'DataFormat','struct');
Easting1 = (cellfun(@(m) double(m.UtmEasting),msgStructs1));
Northing1= ((cellfun(@(m) double(m.UtmNorthing),msgStructs1)));
Altitude1 = cellfun(@(m) double(m.Altitude),msgStructs1);
Fix1 = cellfun(@(m) double(m.Fix),msgStructs1);


bagmov2 = rosbag('/Users/Rushi/EECE5554/Lab2/clearmoving.bag');
msgStructs2 = readMessages(bagmov2,'DataFormat','struct');
Easting2 = (cellfun(@(m) double(m.UtmEasting),msgStructs2));
Northing2= (cellfun(@(m) double(m.UtmNorthing),msgStructs2));
Altitude2 = cellfun(@(m) double(m.Altitude),msgStructs2);
Fix2 = cellfun(@(m) double(m.Fix),msgStructs2);

figure("Name","3d map of moving in rectangle with obstruction")
gscatter3(Easting1,Northing1,Altitude1,Fix1)                        %https://www.mathworks.com/matlabcentral/fileexchange/37970-gscatter3

figure("Name","3d map of moving in rectangle without obstruction")
gscatter3(Easting2,Northing2,Altitude2,Fix2)                        %https://www.mathworks.com/matlabcentral/fileexchange/37970-gscatter3

figure('Name','2d map of Moving Obstructed Data ')
gscatter(Easting1,Northing1,Fix1); 


figure('Name','2d map of moving in rectangle without obstruction')
gscatter(Easting2,Northing2,Fix2);
